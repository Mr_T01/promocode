package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int findPromoCode(long[] list, long searchValue) {
		for (int i = 0; i < list.length; i++) {
			if (list[i] == searchValue)
				return i;
		}

		return NOT_FOUND;
	}

	
	
	public static int interpolar(int von, int bis, long x, int[] area){
		if(area[von] > x || area[bis] < x){
			return -1;
		}
		if(von<bis){
			//interpolieren
			int marker = (
					von + (int)((double)(x - area[von])
					/ (area[bis] - area[von])
					* (bis - von))
			);
			//gesuchte zahl gefunden?
			if(area[marker] == x) return marker;
			//links weitersuchen
			if(area[marker] > x)
				return interpolar(von, marker-1, x, area);
			//rechts weitersuchen
			else return interpolar(marker+1, bis, x, area);
		}
		return NOT_FOUND;
	}
	
	
	public static long fastsearch(int von, int bis, long x, int[] A) {
		
		if(von <= bis) {
			int mB = (von + bis)/2;
			int mI = (int) (von + ((bis - von) * (x - A[von] / (A[bis] - A[von]) )));
			
			if(mB > mI) {
				//vertausche(mB, mI, A);
				int temp;
				temp = mI;
				mI = mB;
				mB = mI;
			}
			
			if(x == A[mB]) {
				return mB;
				
			} else if(x == A[mI]) {
				return mI;
				
			} else if(x < A[mB]) {
				fastsearch(von, mB-1, x, A);
				
			} else if(x < A[mI]) {
				fastsearch(mB+1, mI-1, x, A);
				
			} else {
				fastsearch(mI+1, bis, x, A);
			}
		}
		
		return NOT_FOUND;
		
	}
	
}
