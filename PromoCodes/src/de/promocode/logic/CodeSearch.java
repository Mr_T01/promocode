package de.promocode.logic;

public class CodeSearch {

	public static int NOT_FOUND = -1;

	/**
	 * Sucht einen Code in der Liste.
	 * 
	 * @param list
	 *            Liste der Codes im Long-Format
	 * @param searchValue
	 *            Gesuchter Code
	 * @return Index des Codes, wenn er vorhanden ist, NOT_FOUND wenn er nicht
	 *         gefunden werden konnte
	 */
	public static int findPromoCode(long[] list, long searchValue) {
		for (int i = 0; i < list.length; i++) {
			if (list[i] == searchValue)
				return i;
		}

		return NOT_FOUND;
	}

	public static long fastsearch(long von, long bis, long x, long[] A) {
		
		if(von <= bis) {
			long mB = (von + bis)/2;
			long mI = von + ((bis - von) * (x - A[(int) von] / (A[(int) bis] - A[(int) von]) ));
			
			if(mB > mI) {
				//vertausche(mB, mI, A);
				long temp;
				temp = mI;
				mI = mB;
				mB = mI;
			}
			
			if(x == A[(int) mB]) {
				return mB;
				
			} else if(x == A[(int) mI]) {
				return mI;
				
			} else if(x < A[(int) mB]) {
				fastsearch(von, mB-1, x, A);
				
			} else if(x < A[(int) mI]) {
				fastsearch(mB+1, mI-1, x, A);
				
			} else {
				fastsearch(mI+1, bis, x, A);
			}
		}
		
		return NOT_FOUND;
		
	}
}
